# my simple blog

#### requirements
 - ansible
 - debian 11

#### install
    git clone git@gitlab.com:piatrouAM/blog_deploy.git
    cd blog_deploy
    cp config.yml.sample config.yml
    cp inventory.yaml.sample inventory.yaml

    vim config.yml

    vim inventory.yaml

    ansible-playbook blog.yml -i inventory.yaml